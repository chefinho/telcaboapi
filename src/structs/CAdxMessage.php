<?php

namespace API\Telcabo\structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CAdxMessage structs
 * @subpackage Structs
 */
class CAdxMessage extends AbstractStructBase
{
    /**
     * The message
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $message;
    /**
     * The type
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $type;
    /**
     * Constructor method for CAdxMessage
     * @uses CAdxMessage::setMessage()
     * @uses CAdxMessage::setType()
     * @param string $message
     * @param string $type
     */
    public function __construct($message = null, $type = null)
    {
        $this
            ->setMessage($message)
            ->setType($type);
    }
    /**
     * Get message value
     * @return string|null
     */
    public function getMessage()
    {
        return $this->message;
    }
    /**
     * Set message value
     * @param string $message
     * @return \API\Telcabo\structs\CAdxMessage
     */
    public function setMessage($message = null)
    {
        $this->message = $message;
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \API\Telcabo\structs\CAdxMessage
     */
    public function setType($type = null)
    {
        $this->type = $type;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \API\Telcabo\structs\CAdxMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
