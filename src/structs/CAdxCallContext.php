<?php

namespace API\Telcabo\structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CAdxCallContext structs
 * @subpackage Structs
 */
class CAdxCallContext extends AbstractStructBase
{
    /**
     * The codeLang
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $codeLang;
    /**
     * The poolAlias
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $poolAlias;
    /**
     * The poolId
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $poolId;
    /**
     * The requestConfig
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $requestConfig;
    /**
     * Constructor method for CAdxCallContext
     * @uses CAdxCallContext::setCodeLang()
     * @uses CAdxCallContext::setPoolAlias()
     * @uses CAdxCallContext::setPoolId()
     * @uses CAdxCallContext::setRequestConfig()
     * @param string $codeLang
     * @param string $poolAlias
     * @param string $poolId
     * @param string $requestConfig
     */
    public function __construct($codeLang = null, $poolAlias = null, $poolId = null, $requestConfig = null)
    {
        $this
            ->setCodeLang($codeLang)
            ->setPoolAlias($poolAlias)
            ->setPoolId($poolId)
            ->setRequestConfig($requestConfig);
    }
    /**
     * Get codeLang value
     * @return string|null
     */
    public function getCodeLang()
    {
        return $this->codeLang;
    }
    /**
     * Set codeLang value
     * @param string $codeLang
     * @return \API\Telcabo\structs\CAdxCallContext
     */
    public function setCodeLang($codeLang = null)
    {
        $this->codeLang = $codeLang;
        return $this;
    }
    /**
     * Get poolAlias value
     * @return string|null
     */
    public function getPoolAlias()
    {
        return $this->poolAlias;
    }
    /**
     * Set poolAlias value
     * @param string $poolAlias
     * @return \API\Telcabo\structs\CAdxCallContext
     */
    public function setPoolAlias($poolAlias = null)
    {
        $this->poolAlias = $poolAlias;
        return $this;
    }
    /**
     * Get poolId value
     * @return string|null
     */
    public function getPoolId()
    {
        return $this->poolId;
    }
    /**
     * Set poolId value
     * @param string $poolId
     * @return \API\Telcabo\structs\CAdxCallContext
     */
    public function setPoolId($poolId = null)
    {
        $this->poolId = $poolId;
        return $this;
    }
    /**
     * Get requestConfig value
     * @return string|null
     */
    public function getRequestConfig()
    {
        return $this->requestConfig;
    }
    /**
     * Set requestConfig value
     * @param string $requestConfig
     * @return \API\Telcabo\structs\CAdxCallContext
     */
    public function setRequestConfig($requestConfig = null)
    {
        $this->requestConfig = $requestConfig;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \API\Telcabo\structs\CAdxCallContext
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
