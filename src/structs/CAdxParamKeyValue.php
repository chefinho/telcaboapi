<?php

namespace API\Telcabo\structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CAdxParamKeyValue structs
 * @subpackage Structs
 */
class CAdxParamKeyValue extends AbstractStructBase
{
    /**
     * The key
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $key;
    /**
     * The value
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $value;
    /**
     * Constructor method for CAdxParamKeyValue
     * @uses CAdxParamKeyValue::setKey()
     * @uses CAdxParamKeyValue::setValue()
     * @param string $key
     * @param string $value
     */
    public function __construct($key = null, $value = null)
    {
        $this
            ->setKey($key)
            ->setValue($value);
    }
    /**
     * Get key value
     * @return string|null
     */
    public function getKey()
    {
        return $this->key;
    }
    /**
     * Set key value
     * @param string $key
     * @return \API\Telcabo\structs\CAdxParamKeyValue
     */
    public function setKey($key = null)
    {
        $this->key = $key;
        return $this;
    }
    /**
     * Get value value
     * @return string|null
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * Set value value
     * @param string $value
     * @return \API\Telcabo\structs\CAdxParamKeyValue
     */
    public function setValue($value = null)
    {
        $this->value = $value;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \API\Telcabo\structs\CAdxParamKeyValue
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
