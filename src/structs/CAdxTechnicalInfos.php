<?php

namespace API\Telcabo\structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CAdxTechnicalInfos structs
 * @subpackage Structs
 */
class CAdxTechnicalInfos extends AbstractStructBase
{
    /**
     * The busy
     * @var bool
     */
    public $busy;
    /**
     * The changeLanguage
     * @var bool
     */
    public $changeLanguage;
    /**
     * The changeUserId
     * @var bool
     */
    public $changeUserId;
    /**
     * The flushAdx
     * @var bool
     */
    public $flushAdx;
    /**
     * The loadWebsDuration
     * @var float
     */
    public $loadWebsDuration;
    /**
     * The nbDistributionCycle
     * @var int
     */
    public $nbDistributionCycle;
    /**
     * The poolDistribDuration
     * @var float
     */
    public $poolDistribDuration;
    /**
     * The poolEntryIdx
     * @var int
     */
    public $poolEntryIdx;
    /**
     * The poolExecDuration
     * @var float
     */
    public $poolExecDuration;
    /**
     * The poolRequestDuration
     * @var float
     */
    public $poolRequestDuration;
    /**
     * The poolWaitDuration
     * @var float
     */
    public $poolWaitDuration;
    /**
     * The processReport
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $processReport;
    /**
     * The processReportSize
     * @var int
     */
    public $processReportSize;
    /**
     * The reloadWebs
     * @var bool
     */
    public $reloadWebs;
    /**
     * The resumitAfterDBOpen
     * @var bool
     */
    public $resumitAfterDBOpen;
    /**
     * The rowInDistribStack
     * @var int
     */
    public $rowInDistribStack;
    /**
     * The totalDuration
     * @var float
     */
    public $totalDuration;
    /**
     * The traceRequest
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $traceRequest;
    /**
     * The traceRequestSize
     * @var int
     */
    public $traceRequestSize;
    /**
     * Constructor method for CAdxTechnicalInfos
     * @uses CAdxTechnicalInfos::setBusy()
     * @uses CAdxTechnicalInfos::setChangeLanguage()
     * @uses CAdxTechnicalInfos::setChangeUserId()
     * @uses CAdxTechnicalInfos::setFlushAdx()
     * @uses CAdxTechnicalInfos::setLoadWebsDuration()
     * @uses CAdxTechnicalInfos::setNbDistributionCycle()
     * @uses CAdxTechnicalInfos::setPoolDistribDuration()
     * @uses CAdxTechnicalInfos::setPoolEntryIdx()
     * @uses CAdxTechnicalInfos::setPoolExecDuration()
     * @uses CAdxTechnicalInfos::setPoolRequestDuration()
     * @uses CAdxTechnicalInfos::setPoolWaitDuration()
     * @uses CAdxTechnicalInfos::setProcessReport()
     * @uses CAdxTechnicalInfos::setProcessReportSize()
     * @uses CAdxTechnicalInfos::setReloadWebs()
     * @uses CAdxTechnicalInfos::setResumitAfterDBOpen()
     * @uses CAdxTechnicalInfos::setRowInDistribStack()
     * @uses CAdxTechnicalInfos::setTotalDuration()
     * @uses CAdxTechnicalInfos::setTraceRequest()
     * @uses CAdxTechnicalInfos::setTraceRequestSize()
     * @param bool $busy
     * @param bool $changeLanguage
     * @param bool $changeUserId
     * @param bool $flushAdx
     * @param float $loadWebsDuration
     * @param int $nbDistributionCycle
     * @param float $poolDistribDuration
     * @param int $poolEntryIdx
     * @param float $poolExecDuration
     * @param float $poolRequestDuration
     * @param float $poolWaitDuration
     * @param string $processReport
     * @param int $processReportSize
     * @param bool $reloadWebs
     * @param bool $resumitAfterDBOpen
     * @param int $rowInDistribStack
     * @param float $totalDuration
     * @param string $traceRequest
     * @param int $traceRequestSize
     */
    public function __construct($busy = null, $changeLanguage = null, $changeUserId = null, $flushAdx = null, $loadWebsDuration = null, $nbDistributionCycle = null, $poolDistribDuration = null, $poolEntryIdx = null, $poolExecDuration = null, $poolRequestDuration = null, $poolWaitDuration = null, $processReport = null, $processReportSize = null, $reloadWebs = null, $resumitAfterDBOpen = null, $rowInDistribStack = null, $totalDuration = null, $traceRequest = null, $traceRequestSize = null)
    {
        $this
            ->setBusy($busy)
            ->setChangeLanguage($changeLanguage)
            ->setChangeUserId($changeUserId)
            ->setFlushAdx($flushAdx)
            ->setLoadWebsDuration($loadWebsDuration)
            ->setNbDistributionCycle($nbDistributionCycle)
            ->setPoolDistribDuration($poolDistribDuration)
            ->setPoolEntryIdx($poolEntryIdx)
            ->setPoolExecDuration($poolExecDuration)
            ->setPoolRequestDuration($poolRequestDuration)
            ->setPoolWaitDuration($poolWaitDuration)
            ->setProcessReport($processReport)
            ->setProcessReportSize($processReportSize)
            ->setReloadWebs($reloadWebs)
            ->setResumitAfterDBOpen($resumitAfterDBOpen)
            ->setRowInDistribStack($rowInDistribStack)
            ->setTotalDuration($totalDuration)
            ->setTraceRequest($traceRequest)
            ->setTraceRequestSize($traceRequestSize);
    }
    /**
     * Get busy value
     * @return bool|null
     */
    public function getBusy()
    {
        return $this->busy;
    }
    /**
     * Set busy value
     * @param bool $busy
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setBusy($busy = null)
    {
        $this->busy = $busy;
        return $this;
    }
    /**
     * Get changeLanguage value
     * @return bool|null
     */
    public function getChangeLanguage()
    {
        return $this->changeLanguage;
    }
    /**
     * Set changeLanguage value
     * @param bool $changeLanguage
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setChangeLanguage($changeLanguage = null)
    {
        $this->changeLanguage = $changeLanguage;
        return $this;
    }
    /**
     * Get changeUserId value
     * @return bool|null
     */
    public function getChangeUserId()
    {
        return $this->changeUserId;
    }
    /**
     * Set changeUserId value
     * @param bool $changeUserId
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setChangeUserId($changeUserId = null)
    {
        $this->changeUserId = $changeUserId;
        return $this;
    }
    /**
     * Get flushAdx value
     * @return bool|null
     */
    public function getFlushAdx()
    {
        return $this->flushAdx;
    }
    /**
     * Set flushAdx value
     * @param bool $flushAdx
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setFlushAdx($flushAdx = null)
    {
        $this->flushAdx = $flushAdx;
        return $this;
    }
    /**
     * Get loadWebsDuration value
     * @return float|null
     */
    public function getLoadWebsDuration()
    {
        return $this->loadWebsDuration;
    }
    /**
     * Set loadWebsDuration value
     * @param float $loadWebsDuration
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setLoadWebsDuration($loadWebsDuration = null)
    {
        $this->loadWebsDuration = $loadWebsDuration;
        return $this;
    }
    /**
     * Get nbDistributionCycle value
     * @return int|null
     */
    public function getNbDistributionCycle()
    {
        return $this->nbDistributionCycle;
    }
    /**
     * Set nbDistributionCycle value
     * @param int $nbDistributionCycle
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setNbDistributionCycle($nbDistributionCycle = null)
    {
        $this->nbDistributionCycle = $nbDistributionCycle;
        return $this;
    }
    /**
     * Get poolDistribDuration value
     * @return float|null
     */
    public function getPoolDistribDuration()
    {
        return $this->poolDistribDuration;
    }
    /**
     * Set poolDistribDuration value
     * @param float $poolDistribDuration
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setPoolDistribDuration($poolDistribDuration = null)
    {
        $this->poolDistribDuration = $poolDistribDuration;
        return $this;
    }
    /**
     * Get poolEntryIdx value
     * @return int|null
     */
    public function getPoolEntryIdx()
    {
        return $this->poolEntryIdx;
    }
    /**
     * Set poolEntryIdx value
     * @param int $poolEntryIdx
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setPoolEntryIdx($poolEntryIdx = null)
    {
        $this->poolEntryIdx = $poolEntryIdx;
        return $this;
    }
    /**
     * Get poolExecDuration value
     * @return float|null
     */
    public function getPoolExecDuration()
    {
        return $this->poolExecDuration;
    }
    /**
     * Set poolExecDuration value
     * @param float $poolExecDuration
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setPoolExecDuration($poolExecDuration = null)
    {
        $this->poolExecDuration = $poolExecDuration;
        return $this;
    }
    /**
     * Get poolRequestDuration value
     * @return float|null
     */
    public function getPoolRequestDuration()
    {
        return $this->poolRequestDuration;
    }
    /**
     * Set poolRequestDuration value
     * @param float $poolRequestDuration
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setPoolRequestDuration($poolRequestDuration = null)
    {
        $this->poolRequestDuration = $poolRequestDuration;
        return $this;
    }
    /**
     * Get poolWaitDuration value
     * @return float|null
     */
    public function getPoolWaitDuration()
    {
        return $this->poolWaitDuration;
    }
    /**
     * Set poolWaitDuration value
     * @param float $poolWaitDuration
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setPoolWaitDuration($poolWaitDuration = null)
    {
        $this->poolWaitDuration = $poolWaitDuration;
        return $this;
    }
    /**
     * Get processReport value
     * @return string|null
     */
    public function getProcessReport()
    {
        return $this->processReport;
    }
    /**
     * Set processReport value
     * @param string $processReport
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setProcessReport($processReport = null)
    {
        $this->processReport = $processReport;
        return $this;
    }
    /**
     * Get processReportSize value
     * @return int|null
     */
    public function getProcessReportSize()
    {
        return $this->processReportSize;
    }
    /**
     * Set processReportSize value
     * @param int $processReportSize
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setProcessReportSize($processReportSize = null)
    {
        $this->processReportSize = $processReportSize;
        return $this;
    }
    /**
     * Get reloadWebs value
     * @return bool|null
     */
    public function getReloadWebs()
    {
        return $this->reloadWebs;
    }
    /**
     * Set reloadWebs value
     * @param bool $reloadWebs
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setReloadWebs($reloadWebs = null)
    {
        $this->reloadWebs = $reloadWebs;
        return $this;
    }
    /**
     * Get resumitAfterDBOpen value
     * @return bool|null
     */
    public function getResumitAfterDBOpen()
    {
        return $this->resumitAfterDBOpen;
    }
    /**
     * Set resumitAfterDBOpen value
     * @param bool $resumitAfterDBOpen
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setResumitAfterDBOpen($resumitAfterDBOpen = null)
    {
        $this->resumitAfterDBOpen = $resumitAfterDBOpen;
        return $this;
    }
    /**
     * Get rowInDistribStack value
     * @return int|null
     */
    public function getRowInDistribStack()
    {
        return $this->rowInDistribStack;
    }
    /**
     * Set rowInDistribStack value
     * @param int $rowInDistribStack
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setRowInDistribStack($rowInDistribStack = null)
    {
        $this->rowInDistribStack = $rowInDistribStack;
        return $this;
    }
    /**
     * Get totalDuration value
     * @return float|null
     */
    public function getTotalDuration()
    {
        return $this->totalDuration;
    }
    /**
     * Set totalDuration value
     * @param float $totalDuration
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setTotalDuration($totalDuration = null)
    {
        $this->totalDuration = $totalDuration;
        return $this;
    }
    /**
     * Get traceRequest value
     * @return string|null
     */
    public function getTraceRequest()
    {
        return $this->traceRequest;
    }
    /**
     * Set traceRequest value
     * @param string $traceRequest
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setTraceRequest($traceRequest = null)
    {
        $this->traceRequest = $traceRequest;
        return $this;
    }
    /**
     * Get traceRequestSize value
     * @return int|null
     */
    public function getTraceRequestSize()
    {
        return $this->traceRequestSize;
    }
    /**
     * Set traceRequestSize value
     * @param int $traceRequestSize
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public function setTraceRequestSize($traceRequestSize = null)
    {
        $this->traceRequestSize = $traceRequestSize;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
