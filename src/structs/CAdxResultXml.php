<?php

namespace API\Telcabo\structs;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CAdxResultXml structs
 * @subpackage Structs
 */
class CAdxResultXml extends AbstractStructBase
{
    /**
     * The messages
     * Meta informations extracted from the WSDL
     * - arrayType: wss:CAdxMessage[]
     * - base: soapenc:Array
     * - nillable: true
     * - ref: soapenc:arrayType
     * @var \API\Telcabo\structs\CAdxMessage[]
     */
    public $messages;
    /**
     * The resultXml
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $resultXml;
    /**
     * The status
     * @var int
     */
    public $status;
    /**
     * The technicalInfos
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \API\Telcabo\structs\CAdxTechnicalInfos
     */
    public $technicalInfos;
    /**
     * Constructor method for CAdxResultXml
     * @uses CAdxResultXml::setMessages()
     * @uses CAdxResultXml::setResultXml()
     * @uses CAdxResultXml::setStatus()
     * @uses CAdxResultXml::setTechnicalInfos()
     * @param \API\Telcabo\structs\CAdxMessage[] $messages
     * @param string $resultXml
     * @param int $status
     * @param \API\Telcabo\structs\CAdxTechnicalInfos $technicalInfos
     */
    public function __construct(array $messages = array(), $resultXml = null, $status = null, \API\Telcabo\structs\CAdxTechnicalInfos $technicalInfos = null)
    {
        $this
            ->setMessages($messages)
            ->setResultXml($resultXml)
            ->setStatus($status)
            ->setTechnicalInfos($technicalInfos);
    }
    /**
     * Get messages value
     * @return \API\Telcabo\structs\CAdxMessage[]|null
     */
    public function getMessages()
    {
        return $this->messages;
    }
    /**
     * Set messages value
     * @param \API\Telcabo\structs\CAdxMessage[] $messages
     * @return \API\Telcabo\structs\CAdxResultXml
     */
    public function setMessages(array $messages = array())
    {
        $this->messages = $messages;
        return $this;
    }
    /**
     * Add item to messages value
     * @throws \InvalidArgumentException
     * @param \API\Telcabo\structs\CAdxMessage $item
     * @return \API\Telcabo\structs\CAdxResultXml
     */
    public function addToMessages(\API\Telcabo\structs\CAdxMessage $item)
    {
        $this->messages[] = $item;
        return $this;
    }
    /**
     * Get resultXml value
     * @return string|null
     */
    public function getResultXml()
    {
        return $this->resultXml;
    }
    /**
     * Set resultXml value
     * @param string $resultXml
     * @return \API\Telcabo\structs\CAdxResultXml
     */
    public function setResultXml($resultXml = null)
    {
        $this->resultXml = $resultXml;
        return $this;
    }
    /**
     * Get status value
     * @return int|null
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Set status value
     * @param int $status
     * @return \API\Telcabo\structs\CAdxResultXml
     */
    public function setStatus($status = null)
    {
        $this->status = $status;
        return $this;
    }
    /**
     * Get technicalInfos value
     * @return \API\Telcabo\structs\CAdxTechnicalInfos|null
     */
    public function getTechnicalInfos()
    {
        return $this->technicalInfos;
    }
    /**
     * Set technicalInfos value
     * @param \API\Telcabo\structs\CAdxTechnicalInfos $technicalInfos
     * @return \API\Telcabo\structs\CAdxResultXml
     */
    public function setTechnicalInfos(\API\Telcabo\structs\CAdxTechnicalInfos $technicalInfos = null)
    {
        $this->technicalInfos = $technicalInfos;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \API\Telcabo\structs\CAdxResultXml
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
