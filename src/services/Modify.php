<?php

namespace API\Telcabo\services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Modify services
 * @subpackage Services
 */
class Modify extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named modify
     * Meta informations extracted from the WSDL
     * - documentation: Update X3 object
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \API\Telcabo\structs\CAdxCallContext $callContext
     * @param string $publicName
     * @param string $objectKeys
     * @param string $objectXml
     * @return \API\Telcabo\structs\CAdxResultXml|bool
     */
    public function modify(\API\Telcabo\structs\CAdxCallContext $callContext, $publicName, $objectKeys, $objectXml)
    {
        try {
            $this->setResult($this->getSoapClient()->modify($callContext, $publicName, $objectKeys, $objectXml));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \API\Telcabo\structs\CAdxResultXml
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
