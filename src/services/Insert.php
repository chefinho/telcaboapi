<?php

namespace API\Telcabo\services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Insert services
 * @subpackage Services
 */
class Insert extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named insertLines
     * Meta informations extracted from the WSDL
     * - documentation: NOT YET IMPLEMENTED !!!
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \API\Telcabo\structs\CAdxCallContext $callContext
     * @param string $publicName
     * @param string $objectKeys
     * @param string $blocKey
     * @param string $lineKey
     * @param string $lineXml
     * @return \API\Telcabo\structs\CAdxResultXml|bool
     */
    public function insertLines(\API\Telcabo\structs\CAdxCallContext $callContext, $publicName, $objectKeys, $blocKey, $lineKey, $lineXml)
    {
        try {
            $this->setResult($this->getSoapClient()->insertLines($callContext, $publicName, $objectKeys, $blocKey, $lineKey, $lineXml));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \API\Telcabo\structs\CAdxResultXml
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
