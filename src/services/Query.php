<?php

namespace API\Telcabo\services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Query services
 * @subpackage Services
 */
class Query extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named query
     * Meta informations extracted from the WSDL
     * - documentation: Get X3 objects list
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \API\Telcabo\structs\CAdxCallContext $callContext
     * @param string $publicName
     * @param string $objectKeys
     * @param string $listSize
     * @return \API\Telcabo\structs\CAdxResultXml|bool
     */
    public function query(\API\Telcabo\structs\CAdxCallContext $callContext, $publicName, $objectKeys, $listSize)
    {
        try {
            $this->setResult($this->getSoapClient()->query($callContext, $publicName, $objectKeys, $listSize));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \API\Telcabo\structs\CAdxResultXml
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
