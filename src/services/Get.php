<?php

namespace API\Telcabo\services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Get services
 * @subpackage Services
 */
class Get extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named getDescription
     * Meta informations extracted from the WSDL
     * - documentation: Get X3 web service description regarding publication done in GESAWE
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \API\Telcabo\structs\CAdxCallContext $callContext
     * @param string $publicName
     * @return \API\Telcabo\structs\CAdxResultXml|bool
     */
    public function getDescription(\API\Telcabo\structs\CAdxCallContext $callContext, $publicName)
    {
        try {
            $this->setResult($this->getSoapClient()->getDescription($callContext, $publicName));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named getDataXmlSchema
     * Meta informations extracted from the WSDL
     * - documentation: Get X3 web service schema regarding publication done in GESAWE
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \API\Telcabo\structs\CAdxCallContext $callContext
     * @param string $publicName
     * @return \API\Telcabo\structs\CAdxResultXml|bool
     */
    public function getDataXmlSchema(\API\Telcabo\structs\CAdxCallContext $callContext, $publicName)
    {
        try {
            $this->setResult($this->getSoapClient()->getDataXmlSchema($callContext, $publicName));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \API\Telcabo\structs\CAdxResultXml
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
