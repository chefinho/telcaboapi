<?php

namespace API\Telcabo\services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Run services
 * @subpackage Services
 */
class Run extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named run
     * Meta informations extracted from the WSDL
     * - documentation: Run X3 sub program
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \API\Telcabo\structs\CAdxCallContext $callContext
     * @param string $publicName
     * @param string $inputXml
     * @return \API\Telcabo\structs\CAdxResultXml|bool
     */
    public function run(\API\Telcabo\structs\CAdxCallContext $callContext, $publicName, $inputXml)
    {
        try {
            $this->setResult($this->getSoapClient()->run($callContext, $publicName, $inputXml));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \API\Telcabo\structs\CAdxResultXml
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
