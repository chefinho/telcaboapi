<?php

namespace API\Telcabo\services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Read services
 * @subpackage Services
 */
class Read extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named read
     * Meta informations extracted from the WSDL
     * - documentation: Read X3 object
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \API\Telcabo\structs\CAdxCallContext $callContext
     * @param string $publicName
     * @param string $objectKeys
     * @return \API\Telcabo\structs\CAdxResultXml|bool
     */
    public function read(\API\Telcabo\structs\CAdxCallContext $callContext, $publicName, $objectKeys)
    {
        try {
            $this->setResult($this->getSoapClient()->read($callContext, $publicName, $objectKeys));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \API\Telcabo\structs\CAdxResultXml
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
