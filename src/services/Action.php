<?php

namespace API\Telcabo\services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Action services
 * @subpackage Services
 */
class Action extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named actionObject
     * Meta informations extracted from the WSDL
     * - documentation: Execute specific action on X3 object providing XML flow
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \API\Telcabo\structs\CAdxCallContext $callContext
     * @param string $publicName
     * @param string $actionCode
     * @param string $objectXml
     * @return \API\Telcabo\structs\CAdxResultXml|bool
     */
    public function actionObject(\API\Telcabo\structs\CAdxCallContext $callContext, $publicName, $actionCode, $objectXml)
    {
        try {
            $this->setResult($this->getSoapClient()->actionObject($callContext, $publicName, $actionCode, $objectXml));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named actionObjectKeys
     * Meta informations extracted from the WSDL
     * - documentation: Execute specific action on X3 object providing keys
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \API\Telcabo\structs\CAdxCallContext $callContext
     * @param string $publicName
     * @param string $actionCode
     * @param string $objectKeys
     * @return \API\Telcabo\structs\CAdxResultXml|bool
     */
    public function actionObjectKeys(\API\Telcabo\structs\CAdxCallContext $callContext, $publicName, $actionCode, $objectKeys)
    {
        try {
            $this->setResult($this->getSoapClient()->actionObjectKeys($callContext, $publicName, $actionCode, $objectKeys));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \API\Telcabo\structs\CAdxResultXml
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
