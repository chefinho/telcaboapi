<?php

namespace API\Telcabo\services;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Delete services
 * @subpackage Services
 */
class Delete extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named delete
     * Meta informations extracted from the WSDL
     * - documentation: Delete X3 object
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \API\Telcabo\structs\CAdxCallContext $callContext
     * @param string $publicName
     * @param string $objectKeys
     * @return \API\Telcabo\structs\CAdxResultXml|bool
     */
    public function delete(\API\Telcabo\structs\CAdxCallContext $callContext, $publicName, $objectKeys)
    {
        try {
            $this->setResult($this->getSoapClient()->delete($callContext, $publicName, $objectKeys));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named deleteLines
     * Meta informations extracted from the WSDL
     * - documentation: Remove lines from X3 object table
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \API\Telcabo\structs\CAdxCallContext $callContext
     * @param string $publicName
     * @param string $objectKeys
     * @param string $blocKey
     * @param string $lineKeys
     * @return \API\Telcabo\structs\CAdxResultXml|bool
     */
    public function deleteLines(\API\Telcabo\structs\CAdxCallContext $callContext, $publicName, $objectKeys, $blocKey, $lineKeys)
    {
        try {
            $this->setResult($this->getSoapClient()->deleteLines($callContext, $publicName, $objectKeys, $blocKey, $lineKeys));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \API\Telcabo\structs\CAdxResultXml
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
