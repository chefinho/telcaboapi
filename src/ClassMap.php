<?php

namespace API\Telcabo;

/**
 * Class which returns the class map definition
 * @package
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get()
    {
        return array(
            'CAdxCallContext' => '\\API\\Telcabo\\structs\\CAdxCallContext',
            'CAdxMessage' => '\\API\\Telcabo\\structs\\CAdxMessage',
            'CAdxTechnicalInfos' => '\\API\\Telcabo\\structs\\CAdxTechnicalInfos',
            'CAdxResultXml' => '\\API\\Telcabo\\structs\\CAdxResultXml',
            'CAdxParamKeyValue' => '\\API\\Telcabo\\structs\\CAdxParamKeyValue',
        );
    }
}
