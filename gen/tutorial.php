<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the fist needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientbase class each generated ServiceType class extends this class
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = array(
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'http://erp.telcabo.com/soap-wsdl/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC?wsdl',
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * );
 * etc....
 * ################################################################################
 * Don't forget to add wsdltophp/packagebase:dev-master to your main composer.json.
 * ################################################################################
 */
/**
 * Minimal options
 */
$options = array(
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'http://erp.telcabo.com/soap-wsdl/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC?wsdl',
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \API\Telcabo\ClassMap::get(),
);
/**
 * Samples for Run ServiceType
 */
$run = new \API\Telcabo\services\Run($options);
/**
 * Sample call for run operation/method
 */
if ($run->run(new \API\Telcabo\structs\CAdxCallContext(), $publicName, $inputXml) !== false) {
    print_r($run->getResult());
} else {
    print_r($run->getLastError());
}
/**
 * Samples for Save ServiceType
 */
$save = new \API\Telcabo\services\Save($options);
/**
 * Sample call for save operation/method
 */
if ($save->save(new \API\Telcabo\structs\CAdxCallContext(), $publicName, $objectXml) !== false) {
    print_r($save->getResult());
} else {
    print_r($save->getLastError());
}
/**
 * Samples for Delete ServiceType
 */
$delete = new \API\Telcabo\services\Delete($options);
/**
 * Sample call for delete operation/method
 */
if ($delete->delete(new \API\Telcabo\structs\CAdxCallContext(), $publicName, $objectKeys) !== false) {
    print_r($delete->getResult());
} else {
    print_r($delete->getLastError());
}
/**
 * Sample call for deleteLines operation/method
 */
if ($delete->deleteLines(new \API\Telcabo\structs\CAdxCallContext(), $publicName, $objectKeys, $blocKey, $lineKeys) !== false) {
    print_r($delete->getResult());
} else {
    print_r($delete->getLastError());
}
/**
 * Samples for Read ServiceType
 */
$read = new \API\Telcabo\services\Read($options);
/**
 * Sample call for read operation/method
 */
if ($read->read(new \API\Telcabo\structs\CAdxCallContext(), $publicName, $objectKeys) !== false) {
    print_r($read->getResult());
} else {
    print_r($read->getLastError());
}
/**
 * Samples for Query ServiceType
 */
$query = new \API\Telcabo\services\Query($options);
/**
 * Sample call for query operation/method
 */
if ($query->query(new \API\Telcabo\structs\CAdxCallContext(), $publicName, $objectKeys, $listSize) !== false) {
    print_r($query->getResult());
} else {
    print_r($query->getLastError());
}
/**
 * Samples for Get ServiceType
 */
$get = new \API\Telcabo\services\Get($options);
/**
 * Sample call for getDescription operation/method
 */
if ($get->getDescription(new \API\Telcabo\structs\CAdxCallContext(), $publicName) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getDataXmlSchema operation/method
 */
if ($get->getDataXmlSchema(new \API\Telcabo\structs\CAdxCallContext(), $publicName) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Samples for Modify ServiceType
 */
$modify = new \API\Telcabo\services\Modify($options);
/**
 * Sample call for modify operation/method
 */
if ($modify->modify(new \API\Telcabo\structs\CAdxCallContext(), $publicName, $objectKeys, $objectXml) !== false) {
    print_r($modify->getResult());
} else {
    print_r($modify->getLastError());
}
/**
 * Samples for Action ServiceType
 */
$action = new \API\Telcabo\services\Action($options);
/**
 * Sample call for actionObject operation/method
 */
if ($action->actionObject(new \API\Telcabo\structs\CAdxCallContext(), $publicName, $actionCode, $objectXml) !== false) {
    print_r($action->getResult());
} else {
    print_r($action->getLastError());
}
/**
 * Sample call for actionObjectKeys operation/method
 */
if ($action->actionObjectKeys(new \API\Telcabo\structs\CAdxCallContext(), $publicName, $actionCode, $objectKeys) !== false) {
    print_r($action->getResult());
} else {
    print_r($action->getLastError());
}
/**
 * Samples for Insert ServiceType
 */
$insert = new \API\Telcabo\services\Insert($options);
/**
 * Sample call for insertLines operation/method
 */
if ($insert->insertLines(new \API\Telcabo\structs\CAdxCallContext(), $publicName, $objectKeys, $blocKey, $lineKey, $lineXml) !== false) {
    print_r($insert->getResult());
} else {
    print_r($insert->getLastError());
}
