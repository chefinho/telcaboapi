<?php
	require_once ('imports.class.php');

	class TelCaboMethods { 
		
		public function run($parametros_para_enviar) {

            $parametros_para_enviar = (object) $parametros_para_enviar;
			
			$options = array(
                \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'http://erp.telcabo.com/soap-wsdl/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC?wsdl',
                \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \API\Telcabo\ClassMap::get(),
				\WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'WS',
				\WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => '123',
				\WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
				\WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CACHE_WSDL => true,
			);

			libxml_disable_entity_loader(false);
				
			$srv = new \API\Telcabo\services\Run($options);
			// $_get->endpoint = ;

			$callContext = new \API\Telcabo\structs\CAdxCallContext;

			$callContext->setCodeLang($parametros_para_enviar->codeLang)
						->setPoolAlias($parametros_para_enviar->poolAlias)
						->setPoolId($parametros_para_enviar->poolId)
						->setRequestConfig($parametros_para_enviar->requestConfig);
			
			$publicName = $parametros_para_enviar->publicName;
			$inputXml =  $parametros_para_enviar->inputXml;


			$req = $srv->run($callContext, $publicName, $inputXml);
			if ($req !== false) {
				syslog(LOG_INFO, var_export(array( "Resposta" => $req->getLastResponse(), "Request" => $req->getLastRequest() ), true));
			} else {
				syslog(LOG_INFO, var_export(array( "Resposta" => $req->getLastError(), "Request" => $req->getLastRequest() ), true));
			}
    
		}


	}