<?php
    require_once ('src/ClassMap.php');

    $fullPath = 'src/';

    foreach (glob($fullPath . "services/*.php") as $filename) {
        include $fullPath . "services/" . basename($filename);
    }

    foreach (glob($fullPath . "structs/*.php") as $filename) {
        include $fullPath . "structs/" . basename($filename);
    }
